/**
 * Application nodejs pour le laboratoire 13 du cours INF2005 @ UQAM
 * node inf2005lab13.js
 */
var express = require('express');
var app=express();
var bodyParser = require('body-parser');

app.use(express.static(__dirname));

var portNumber=3000;// au besoin changez le numero de port
app.listen(portNumber,function(){console.log(' le serveur fonctionne sur le port: '+portNumber)});
console.log('serveur demarré avec success');